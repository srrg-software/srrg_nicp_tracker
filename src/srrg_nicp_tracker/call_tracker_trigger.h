#pragma once

#include <srrg_messages/sensor_message_sorter.h>
#include <srrg_messages/message_timestamp_synchronizer.h>
#include <srrg_messages/pinhole_image_message.h>

#include "tracker.h"

namespace srrg_nicp_tracker {
  
  class CallTrackerTrigger: public srrg_core::SensorMessageSorter::Trigger{
  public:
    CallTrackerTrigger(srrg_core::SensorMessageSorter* sorter,
		       int priority,
		       Tracker* tracker_,
		       std::vector<srrg_core::MessageTimestampSynchronizer>* synchronizers_=0);
    virtual void action(std::tr1::shared_ptr<srrg_core::BaseSensorMessage> msg);
    
    inline Tracker* tracker() {return _tracker;}
  protected:
    Tracker* _tracker;
    std::vector<srrg_core::MessageTimestampSynchronizer>* _synchronizers;
    srrg_core::PinholeImageMessage* _depth_img, *_rgb_img;
  };
}
