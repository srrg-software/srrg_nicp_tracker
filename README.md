# srrg_nicp_tracker

This package implements a tracker on top of [srrg_nicp](https://gitlab.com/srrg-software/srrg_nicp)

## Prerequisites

requires:
* [srrg_core](https://gitlab.com/srrg-software/srrg_core)
* [srrg_core_map](https://gitlab.com/srrg-software/srrg_core_map)
* [srrg_nicp](https://gitlab.com/srrg-software/srrg_nicp)
* [srrg_cmake_modules](https://gitlab.com/srrg-software/srrg_cmake_modules)


## Authors
* Jacopo Serafin
* Giorgio Grisetti

## Related Publications

* Jacopo Serafin and Giorgio Grisetti. "[**Using extended measurements and scene merging for efficient and robust point cloud registration**](http://www.sciencedirect.com/science/article/pii/S0921889015302712)", Robotics and Autonomous Systems, 2017.
* Jacopo Serafin and Giorgio Grisetti. "[**NICP: Dense Normal Based Point Cloud Registration.**](http://ieeexplore.ieee.org/document/7353455/)" In Proc. of the International Conference on Intelligent Robots and Systems (IROS), Hamburg, Germany, 2015.
